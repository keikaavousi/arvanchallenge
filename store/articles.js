import articleServices from "@/services/articleServices";

export const state = () => {
  return {
    posts: []
  };
};
export const mutations = {
  FETCH_POSTS(state, posts) {
    state.posts = posts;
  },
  ADD_POST(state, post) {
    state.posts = [...state.posts, post];
  },
  EDIT_POST(state, post) {
    let finded = state.posts.findIndex(n => n.slug == post.slug);
    state.posts[finded] = post;
  },
  DELETE_POST(state, slug) {
    state.posts = state.posts.filter(n => n.slug !== slug);
  }
};
export const actions = {
  fetchPosts({ commit }, author) {
    return articleServices.getArticles(author).then(res => {
      commit("FETCH_POSTS", res.data.articles);
    });
  },
  addPost({ commit }, value) {
    return articleServices.addArticle(value).then(res => {
      commit("ADD_POST", res.data);
    });
  },
  editPost({ commit }, value) {
    return articleServices.editArticle(value).then(res => {
      commit("EDIT_POST", value);
    });
  },
  deletePost({ commit }, slug) {
    return articleServices.deleteArticle(slug).then(res => {
      commit("DELETE_POST", slug);
    });
  }
};
export const getters = {
  getArticles: state => {
    return state.posts;
  }
};
