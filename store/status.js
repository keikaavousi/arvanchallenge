export const state = () => {
  return {
    status: false,
    message: ""
  };
};
export const mutations = {
  SET_STATUS(state, msg) {
    state.status = true;
    state.message = msg;
  }
};
export const actions = {
  setStatus({ commit }, value) {
    commit("SET_STATUS", value.msg);
  }
};
export const getters = {
  getStatus: state => {
    return state;
  }
};
