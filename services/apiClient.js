import axios from "axios";
let user = JSON.parse(window.localStorage.getItem("auth"));
let token;
if (user) {
  token = user.token;
}

export const apiClient = axios.create({
  baseURL: process.env.API_URL,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    Authorization: token ? `Token ${token}` : ""
  }
});
