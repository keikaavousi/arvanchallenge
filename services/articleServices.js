import { apiClient } from "./apiClient";

export default {
  getArticles(author) {
    return apiClient.get(`/articles?author=${author}`);
  },
  addArticle(article) {
    console.log(article);
    return apiClient.post("/articles", article);
  },
  editArticle(article) {
    console.log(article);
    return apiClient.put(`/articles/${article.slug}`, article);
  },
  deleteArticle(slug) {
    return apiClient.delete(`/articles/${slug}`);
  }
};
