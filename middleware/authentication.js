export default function({ store, redirect, route }) {
  if (route.fullPath !== "/login" && route.fullPath !== "/register") {
    if (window.localStorage.getItem("auth") == null) {
      return redirect("/login");
    }
  }
}
